from PIL import Image


def color_formula(r, g, b):
    return int(0.299 * r + 0.587 * g + 0.114 * b)


class RgbChangeMatrix(object):
    R = 0
    G = 1
    B = 2

    def __init__(self, initial, final, steps):
        self._initial = initial
        self._final = final
        self._steps = steps
        self._matrix = {}
        self._populate_matrix()

    def _populate_matrix(self):
        for i, j, from_pixel in self._initial.pixels_range():
            to_pixel = self._final.get_pixel(i, j)
            r0, g0, b0 = from_pixel
            r1, g1, b1 = to_pixel
            speed = lambda x0, x1: float(x1 - x0) / self._steps
            self._matrix[self._make_key(i, j)] = speed(r0, r1), speed(g0, g1), speed(b0, b1)

    def _make_key(self, i, j):
        return '{}${}'.format(i, j)

    def nth_application_for_pixel(self, i, j, n):
        matrix_cell = self._matrix[self._make_key(i, j)]
        return (
            int(self._initial.get_pixel(i, j)[self.R] + matrix_cell[self.R] * n),
            int(self._initial.get_pixel(i, j)[self.G] + matrix_cell[self.G] * n),
            int(self._initial.get_pixel(i, j)[self.B] + matrix_cell[self.B] * n),
        )

    def build_nth_transform(self, n):
        new_img = Image.new('RGB', self._initial.size)
        new_pixels = new_img.load()
        for i, j, pixel in self._initial.pixels_range():
            new_pixels[i, j] = self.nth_application_for_pixel(i, j, n)
        return PixelCollection(new_img, self._initial.size)


class PixelCollection(object):
    def __init__(self, img, size):
        self._size = size
        self._img = img
        self._pixels = img.load()

    @property
    def size(self):
        return self._size

    def change_rgb_via_functions(self, r_func, g_func, b_func):
        new_img = Image.new('RGB', self._size)
        new_pixels = new_img.load()
        for i, j, pixel in self.pixels_range():
            new_pixels[i, j] = r_func(*pixel), g_func(*pixel), b_func(*pixel)
        return self.__class__(new_img, self._size)

    def save_to_file(self, file_name):
        self._img.save(file_name)

    def get_pixel(self, i, j):
        return self._pixels[i, j]

    def pixels_range(self):
        for i in range(self._size[0]):
            for j in range(self._size[1]):
                yield i, j, self._pixels[i, j]


steps = 250
img = Image.open('img0.jpg')
initial = PixelCollection(img, img.size)
final = initial.change_rgb_via_functions(color_formula, color_formula, color_formula)
change_matrix = RgbChangeMatrix(initial, final, steps)

current = initial
for i in range(steps):
    print('iteration {}'.format(i))
    current = change_matrix.build_nth_transform(i)
    current.save_to_file('out/img{}.jpg'.format(i + 1))
